const rabbit = require('./rabbit');

exports.sms = (message) => {
  rabbit.publish(
    'amqps://databeaver:databeaver123@portal1085-32.excellent-rabbitmq-22.3574791654.composedb.com:28756/excellent-rabbitmq-22',
    'sms',
    message,
  );
};

exports.email = (message) => {
  rabbit.publish(
    'amqps://databeaver:databeaver123@portal1085-32.excellent-rabbitmq-22.3574791654.composedb.com:28756/excellent-rabbitmq-22',
    'email',
    message,
  );
};
