const amqp = require('amqplib/callback_api');

const url = require('url');

exports.publish = (connector, q, message) => {
  const uri = connector;
  const parsedURI = url.parse(uri);
  amqp.connect(
    uri,
    { servername: parsedURI.hostname },
    (err, conn) => {
      conn.createChannel((errr, ch) => {
        console.log(errr);
        ch.assertQueue(q, { durable: true });
        // Note: on Node 6 Buffer.from(msg) should be used
        ch.sendToQueue(q, Buffer.from(message));
        console.log('[x] Sent to queue: ', q);
      });
    },
  );
};

exports.consume = (connector, q, cb) => {
  const uri = connector;
  const parsedURI = url.parse(uri);

  return new Promise((resolve, reject) => {
    amqp.connect(
      uri,
      { servername: parsedURI.hostname },
      (err, conn) => {
        if (err) throw err;
        conn.createChannel((err, ch) => {
          console.log(err);
          ch.assertQueue(q, { durable: true });
          console.log(
            ' [*] Waiting for messages in %s. To exit press CTRL+C',
            q,
          );
          ch.consume(
            q,
            (msg) => {
              // console.log(" [x] Received %s", msg.content.toString());
              cb(msg.content.toString());
              return resolve(msg.content.toString());
            },
            { noAck: false },
          );
        });
      },
    );
  });
};
