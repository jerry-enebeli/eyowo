const amqp = require('amqplib/callback_api');
const url = require('url');

require('dotenv').config();

const parsedURI = url.parse(process.env.AMQP_HOST1);
console.log(parsedURI);


module.exports = {
  // attachments = array of object attachments. more info: https://nodemailer.com/using-attachments/
  send(recipientEmail, subject, htmlmessage) {
    amqp.connect(process.env.AMQP_HOST1, { servername: parsedURI.hostname }, (err, conn) => {
      conn.createChannel((errr, ch) => {
        if (errr) {
          console.error(err);
          return;
        }
        const data = {
          recipientEmail,
          subject,
          htmlmessage,
        };
        const q = 'email';
        const msg = JSON.stringify(data);

        ch.assertQueue(q, { durable: true });
        ch.sendToQueue(q, Buffer.from(msg), { persistent: true });
        console.log(" [x] Sent '%s'", msg);
      });
    });
  },
};
