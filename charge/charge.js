const grpc = require('grpc');

const PRORTO_PATH = `${__dirname}/charge.proto`;

const { EyowoCharge } = grpc.load(PRORTO_PATH);


const client = new EyowoCharge.Charge('35.239.28.92:80', grpc.credentials.createInsecure());

// const client = new EyowoCharge.Charge('0.0.0.0:50053', grpc.credentials.createInsecure());

exports.GenerateCharge = async (code, action, data) => new Promise((resolve) => {
  client.GenerateCharge({ code, action, data: JSON.stringify(data) }, (err, resp) => {
    console.log(err);
    resp.data = JSON.parse(resp.data);
    resolve(resp.data);
  });
});

