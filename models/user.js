// grab the packages that we need for the user model
const mongoose = require('mongoose');
const hash = require('../util/crypto.js');

const { Schema } = mongoose;

mongoose.Promise = require('bluebird');

// user schema
const UserSchema = new Schema({
  name: {
    fname: { type: String, required: false },
    lname: { type: String, required: false },
  },
  merchant: { type: Boolean, default: false },
  newuser: { type: Boolean, required: true, default: false },
  verified: { type: Boolean, required: true, default: false },
  email: { type: String, required: false, index: true },
  mobile: { type: String, required: true, index: { unique: true } },
  secure_pin: { type: String, required: false },
  eyowo_pin: { type: String, required: false },
  firebase_tokens: [{ type: String }],
  recovery: { type: String },
  /*
  firebase_tokens: [{
    _id: false,
    device: { type: String, required: true, enum: ['iOS', 'Android'] },
    device_id: { type: String, required: true },
    token: { type: String, required: true },
  }], */
  date_created: { type: Date, default: Date.now },
  last_login: Date,
  wallet: { type: Number, default: 0, required: true },
  bank_accounts: [{
    account_number: { type: String, required: true },
    bank_code: { type: String, required: true },
    fname: { type: String, required: true },
    lname: { type: String, required: true },
    passcode: { type: String, required: true },
    email: { type: String, required: true },
    phone: { type: String, required: true },
    cardno: { type: String, required: false },
  }],
  beneficiaries: [{
    account_number: { type: String, required: true },
    bank_code: { type: String, required: true },
    account_name: { type: String, required: true },
  }],
  settings: {
    errands: { type: Boolean, default: false },
    chat: { type: Boolean, default: false },
  },
  cards: [{
    pan1: { type: String, required: true }, // 1st 4 characters of card
    pan2: { type: String, required: true }, // last 4 characters of card
    instrument_id: { type: String, required: true },
  }],
  /*
  authorizations: [{
    from: { type: String, required: true },
    amount: { type: String, required: true },
    date: { type: Date, required: true, default: Date.now },
  }], */
});
UserSchema.pre('save', function hashSecurePin(next) {
  const user = this;

  // hash the password only if the password has been changed or admin is new
  if (!user.isModified('secure_pin')) return next();

  const hashedPin = hash.saltHashPassword(user.secure_pin, user.mobile);
  user.secure_pin = hashedPin;
  next();

  return null;
});


// method to compare a given password with the database hash
UserSchema.methods.compareSecurePin = function compareSecurePin(pin) {
  const user = this;
  const hashedPin = hash.saltHashPassword(pin, user.mobile);
  return (user.secure_pin === hashedPin);
  // return bcrypt.compareSync(pin, user.secure_pin);
};

// return the schema
module.exports = mongoose.model('User', UserSchema);
